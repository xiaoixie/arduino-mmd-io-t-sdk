# Arduino TopLevel Client for MMD IoT Platform

 `MMDIoTSDK` 可以帮助你快速连接物联网平台

> 可以手动把 gitlab 上的项目 clone 下来，放到 arduino 的 library 库下，便可使用。

> 没有权限的请联系管理员开git权限

## Usage 使用示例

```c++
//引入 MMD IoT SDK
#include <MMDIoTSDK.h>

//引入 WIFI模块
#include <ESP8266WiFi.h>
//WIFI模块实例化
static WiFiClient espClient;

//WIFI的SSID和密码
#define WiFI_SSID "WIFI账户"
#define WIFI_PASSWD "WIFI密码"

//mqtt服务地址
#define MQTT_SERVER "127.0.0.1"
//设备信息
#define PRODUCT_KEY "Zx5uKWiETW.."
#define DEVICE_NAME  "qLn5kjb8SDUrajWQ.."
#define DEVICE_SECRET  "9e3a750fcc7f4d3.."

void setup() {
    //定义针脚为输出信号
    pinMode(BUILTIN_LED, OUTPUT);
    pinMode(4, OUTPUT); 
    pinMode(12, OUTPUT); 
    pinMode(14, OUTPUT); 
    pinMode(16, OUTPUT); 

    Serial.begin(115200); 
    //执行Wifi初始化，下文有具体描述
    initWIFI(WiFI_SSID, WIFI_PASSWD);
    
    // 初始化 iot，需传入 wifi 的 client，和 mqtt服务ip或者域名，以及 设备信息                     
    MMDIoTSDK::begin(espClient, MQTT_SERVER, PRODUCT_KEY, DEVICE_NAME, DEVICE_SECRET);

    // 绑定一个设备属性回调，当远程修改此属性，会触发 lampswitchCallback
    // lampswitch 是在产品物模型中的属性标识符identifier, 同一个产品中具有唯一性
    MMDIoTSDK::bindData("lampswitch", lampswitchCallback);
}

// 灯开关 属性修改的回调函数
void lampswitchCallback(JsonVariant p)
{
    int lampswitch = p["lampswitch"];
    if (lampswitch == 1) {
        digitalWrite(BUILTIN_LED, HIGH);
    } else {
        digitalWrite(BUILTIN_LED, LOW);
    }
    

    MMDIoTSDK::addBuffer("lampswitch", lampswitch);
    MMDIoTSDK::sendBuffer();
}

void loop() {
    MMDIoTSDK::loop();
}

// 初始化WIFI连接
void initWIFI(const char *ssid, const char *password) {
    delay(10);
    // 板子通电后要启动，稍微等待一下让板子点亮
    Serial.println("Connecting to ");
    Serial.print(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(2000);
        Serial.println("WiFi not Connect");
    }
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.print(WiFi.localIP());
}
```

## API 可用方法

```c++
// 在主程序 loop 中调用，检查连接和定时发送Buffer数据
  static void loop();

  /**
   * 初始化程序
   * @param espClient client
   * @param _mqttServer mqtt服务地址
   * @param _productKey 
   * @param _deviceName 
   * @param _deviceSecret
   */
  static void begin(Client &espClient,
                    const char *_mqttServer,
                    const char *_productKey,
                    const char *_deviceName,
                    const char *_deviceSecret);

  /**
   * 发送数据
   * @param param 字符串形式的json 数据，例如 {"${key}":"${value}"}
   */
  static void send(const char *param);

  /**
   * 发送Buffer数据
   */
  static void sendBuffer();

  /**
   * 添加或更新Buffer float 格式数据
   * @param key 数据的 key
   * @param number 数据的值
   */
  static void addBuffer(char *key, float number);

  /**
   * 添加或更新Buffer int 格式数据
   * @param key 数据的 key
   * @param number 数据的值
   */
  static void addBuffer(char *key, int number);

  /**
   * 添加或更新Buffer double 格式数据
   * @param key 数据的 key
   * @param number 数据的值
   */
  static void addBuffer(char *key, double number);

  /**
   * 添加或更新Buffer string 格式数据
   * @param key 数据的 key
   * @param text 数据的值
   */
  static void addBuffer(char *key, char *text);

  /**
   * 删除Buffer 数据
   * @param key 数据的 key
   */
  static void removeBuffer(char *key);

  /**
   * 卸载某个 key 的所有回调（慎用）
   * @param key 物模型的key
   */
  static int unbindData(char *key);

  /**
   * 绑定属性回调，云服务下发的数据包含此 key 会进入回调，用于监听特定数据的下发
   * @param key 物模型的key
   */
  static int bindData(char *key, poniter_fun fp);

  static boolean publish(const char *topic, const char *payload);

  /**
   * 订阅topic
   * @param topic 订阅的topic
   * @param qos 
   */
  static boolean subscribe(char *topic, uint8_t qos);

  /**
   * 订阅topic
   * @param topic 订阅的topic
   * @param fp 回调函数
   */
  static boolean subscribe(char *topic, poniter_fun fp);

  /**
   * 订阅topic
   * @param topic 订阅的topic
   * @param qos 
   * @param fp 回调函数
   */
  static boolean subscribe(char *topic, uint8_t qos, poniter_fun fp);

  /**
   * 取消订阅指定topic
   * @param topic 订阅的topic
   */
  static boolean unsubscribe(char *topic);
```