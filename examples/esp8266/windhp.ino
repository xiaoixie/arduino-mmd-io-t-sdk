//引入 卫宁WinDHP IoT SDK
#include <WinDHPIoTSDK.h>

//引入 WIFI模块
#include <ESP8266WiFi.h>
//WIFI模块实例化
static WiFiClient espClient;

//WIFI的SSID和密码
#define WiFI_SSID "YU-TEST"
#define WIFI_PASSWD "windhppt"

//mqtt服务地址
#define MQTT_SERVER "172.16.9.134"
//设备信息
#define DEVICE_NAME  "qLn5kjb8SDUrajWQqveVCm"
#define PRODUCT_KEY "Zx5uKWiETWaw"
#define DEVICE_SECRET  "9e3a750fcc7f4d3faee64f238f10f82e"

//属性实时JSON数据
StaticJsonDocument<200> realtimeData;

void setup() {
    //定义针脚4为输出信号
    pinMode(4, OUTPUT);

    Serial.begin(115200);

    //执行Wifi初始化，下文有具体描述
    initWIFI(WiFI_SSID, WIFI_PASSWD);

    // 初始化 iot，需传入 wifi 的 client，和 mqtt服务ip或者域名，以及 设备信息
    WinDHPIoTSDK::begin(espClient, MQTT_SERVER, PRODUCT_KEY, DEVICE_NAME, DEVICE_SECRET);

    // 绑定一个设备属性回调，当远程修改此属性，会触发 lampswitchCallback
    // lampswitch 是在产品物模型中的属性标识符identifier, 同一个产品中具有唯一性
    WinDHPIoTSDK::bindData("lampswitch", lampswitchCallback);
}

// 灯开关 属性修改的回调函数
void lampswitchCallback(JsonVariant p)
{
    int lampswitch = p["lampswitch"];
    if (lampswitch == 1) {
        digitalWrite(4, HIGH);
    } else {
        digitalWrite(4, LOW);
    }
    realtimeData["lampswitch"] = lampswitch;
    sendRealtimeData();
}

void loop() {
    WinDHPIoTSDK::loop();
}

//发送实时数据
void sendRealtimeData() {
    Serial.print("publish device info:");
    serializeJson(realtimeData, Serial);
    //发布
    String output;
    serializeJson(realtimeData, output);
    const char *msg = output.c_str();
    WinDHPIoTSDK::send(msg);
}

// 初始化WIFI连接
void initWIFI(const char *ssid, const char *password) {
    delay(10);
    // 板子通电后要启动，稍微等待一下让板子点亮
    Serial.println("Connecting to ");
    Serial.print(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(2000);
        Serial.println("WiFi not Connect");
    }
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.print(WiFi.localIP());
}